"""Tetris gym environment"""
import gym
import random
from gym import error, spaces, utils
from gym.utils import seeding
from gym_tetris.envs.tetris import TetrisGame, BotController

"""
Valid actions are:
down, right, left, rotate, drop, wait
"""

class TetrisEnv(gym.Env):
    metadata = {'render.modes' : ['human']}
    def __init__(self):
        #Maps action number to command string
        self.action_map = {
            0 : 'right',
            1 : 'down',
            2 : 'left',
            3 : 'rotate',
            4 : 'drop',
        }
        #Stores number of actions
        self.action_space = spaces.Discrete(len(self.action_map))
        self.controller = BotController()
        self.tetris_game = TetrisGame(1, self.controller)
        #Stores previous heuristics
        self.previous_lines = 0
        self.previous_bumpiness = 0
        self.previous_aggregate_height = 0
        self.previous_holes = 0

    """This just runs a game step without doing anything"""
    def run_game_step(self):
        self.tetris_game.step()

    """
    Runs a step in the game given an action
    Observation doesnt actually return anything
    pass in the action as an integer, referring to the command
    in the action map
    """
    def step(self, action):
        #Run action
        self.controller.action(self.action_map[action])

        #Runs the next step in the game
        self.tetris_game.step()

        observation = None
        done = self.tetris_game.model.gameover
        reward = self.calculate_reward(done)
        info = {

        }
        return observation, reward, done, info

    """Calculates the reward gained by an action"""
    def calculate_reward(self, done):
        #Calculate score change
        new_lines = self.tetris_game.model.lines_cleared
        lines_change = new_lines - self.previous_lines
        self.previous_lines = new_lines

        aggregate_height, holes, bumpiness = self.tetris_game.get_heuristics()

        #Get height change
        height_change = aggregate_height - self.previous_aggregate_height
        self.previous_aggregate_height = aggregate_height

        #Get holes change
        holes_change = holes - self.previous_holes
        self.previous_holes = holes

        #Get bumpiness change
        bumpiness_change = bumpiness - self.previous_bumpiness
        self.previous_bumpiness = bumpiness

        return 0.76 * new_lines - 0.51 * aggregate_height - 0.36 * holes - 0.18 * bumpiness

    """Resets the Tetris game"""
    def reset(self):
        self.tetris_game.reset()
        self.previous_lines = 0
        self.previous_bumpiness = 0
        self.previous_aggregate_height = 0
        self.previous_holes = 0
        self.tetris_game.step()

    """Returns the frame of the game"""
    def render(self, mode='human'):
        return self.tetris_game.fetch_frame()


if __name__ == '__main__':
    t = TetrisEnv()
    while True:
        actions = ['right', 'left', 'rotate']
        t.step(random.choice(actions))