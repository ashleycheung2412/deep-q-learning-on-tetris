"""This is the tetris bot model"""
import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import random



"""
This is the strategy that the agent uses
Takes in starting and ending value of epsilon and the decay rate
"""
class EpsilonGreedyStrategy():
    def __init__(self, epsilon_start, epsilon_end, decay):
        self.start = epsilon_start
        self.end = epsilon_end
        self.decay = decay

    """
    Returns the exploration rate given the current step
    Formula:
        x_1 + (x_0 - x_1) / e^(dx)
    where x_1 is end, x_0 is start, d is decay rate, and x is step
    """
    def get_exploration_rate(self, current_step):
        return self.end + (self.start - self.end) * math.exp(-1. * current_step * self.decay)


"""
This is the agent that picks the actions
"""
class TetrisAgent():
    def __init__(self, strategy, num_actions, device):
        #Stores how many steps has passed for this agent
        self.current_step = 0
        #This stores the strategy used to pick the action given Q values
        self.strategy = strategy
        #Number of actions that the agent can pick
        self.num_actions = num_actions
        #Device for pytorch
        self.device = device

    """
    Select an action based on the given strategy and a state
    and return the action
    """
    def select_action(self, state, policy_net):
        #Get exploration rate based on current step
        rate = self.strategy.get_exploration_rate(self.current_step)
        #Increase step counter
        self.current_step += 1

        #Decide whether to explore or exploit
        if rate > random.random():
            action = random.randrange(self.num_actions)
            #Explore
            return action
        else:
            #Exploit
            #with torch.no_grad() ensures that just for the next block
            #gradient calculations are disabled to decrease memory use
            with torch.no_grad():
                #Pick the action with the highest value
                q_values = policy_net(state.unsqueeze(0))
                action = q_values.argmax(dim=1).to(self.device).item()
                return action

"""
Deep Q network that emulates the Q table
"""
class TetrisDQN(nn.Module):
    def __init__(self, img_size, num_actions):
        super().__init__()
        self.img_size = img_size
        #In channels are for frames
        self.in_channels = 3
        self.conv1 = nn.Conv2d(self.in_channels, 32, 8, stride=4)
        self.conv2 = nn.Conv2d(32, 64, 4, stride=2)
        self.conv3 = nn.Conv2d(64, 128, 3, stride=1)
        self.fc1 = nn.Linear(in_features=768, out_features=512)
        self.out = nn.Linear(in_features=512, out_features=num_actions)

    """
    Forward method of network
    Takes in a batch of images in the form of a tensor an outputs the q_values
    tensor(batch_size, p1, p2 ....)
    Output:
    2d tensor: tensor(batch_size, qvalues)
    """
    def forward(self, state_tensor):
        #Pass through network
        #Flatten the tensors into a 1 dimensional tensor, ignoring the batch dimension
        #So it will be a 4d tensor(batchsize, rgbchannel, img_width, img_height)
        state_tensor = F.relu(self.conv1(state_tensor))
        state_tensor = F.relu(self.conv2(state_tensor))
        state_tensor = F.relu(self.conv3(state_tensor))
        state_tensor = state_tensor.flatten(start_dim=1)
        state_tensor = F.relu(self.fc1(state_tensor))
        state_tensor = self.out(state_tensor)
        return state_tensor




