"""This is an implementation of the training algorithm"""
import gym
import gym_tetris
import numpy as np
import math
import random
import torch
import torch.nn as nn
import torchvision.transforms as transforms
import torch.optim as optim
from tetris_network import TetrisAgent, TetrisDQN, EpsilonGreedyStrategy
from collections import namedtuple
from PIL import ImageOps


"""Named Experience tuple"""
Experience = namedtuple('Experience', 
    ('state', 'action', 'reward', 'next_state')
)


"""This is a class used to manage replay memory"""
class ReplayMemory:
    def __init__(self, capacity):
        #Number of experiences it stores
        self.capacity = capacity
        #Stores memory in a queue
        self.memory = []

    """Push an experience onto memory"""
    def push(self, experience):
        #Add new experience
        self.memory.append(experience)

        #Check if capacity has been reached
        if len(self.memory) >= self.capacity:
            #Remove first element from queue (oldest memory)
            #to keep under capacity
            self.memory.pop(0)

    """
    Sample experiences randomly from memory
    Returns the batch_size num of randomly selected experiences
    """
    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    """
    Returns whether there is enough experiences to sample given a
    batch size
    """
    def can_sample(self, batch_size):
        return len(self.memory) >= batch_size


"""QValues helper class"""
class QValue:
    @staticmethod
    def get_current(policy_net, states, actions):
        #Get current q values
        curr_qvalues = policy_net(states)
        #Get only the q values for the given actions
        #Note: Unsqueeze must be called because action is tensor in
        #form: tensor([0,1,2,3,...]) but must be in form tensor([0],[1],[2],[3],..)
        return torch.gather(curr_qvalues, 1, actions.unsqueeze(1))

    @staticmethod
    def get_next(target_net, next_states):
        next_qvalues = target_net(next_states)
        #Returns a 1d tensor which contains the maximum of each row
        #in the first dimension (Qvalues dimension)
        return torch.max(next_qvalues,1).values.unsqueeze(1)


"""Hyper-parameters"""
memory_size = 100000
game_img_size = (40,80)
num_episodes = 10000
save_interval = 100    #How many intervals before saving model again
max_episode_len = 10000
epsilon_start = 1
epsilon_end = 0.01
epsilon_decay = 0.00001
batch_size = 256
reward_discount = 0.999
lr = 0.001
target_update = 10  #How many episodes before target_net gets updated again
survival_decay_rate = 1
base_gameover_reward = -1

"""Class that stores the last N frame tensors"""
class FrameQueue:
    def __init__(self, max_frames):
        self.frames = []
        self.max_frames = max_frames

    """Push frame tensor onto frame queue"""
    def push(self, frame):
        #Append the frames
        self.frames.append(frame)
        if len(self.frames) > self.max_frames:
            #Pop latest frame
            self.frames.pop(0)
    
    """Gets a tensor with all the last n frames stacked"""
    def get_frames(self):
        #Return stacked frames if there are enough frames
        if len(self.frames) == self.max_frames:
            return torch.stack(self.frames, dim=0)
        
        #Else duplicate the latest frame till there are enough frames
        output = self.frames
        while len(self.frames) != self.max_frames:
            output.append(self.frames[-1])
        return torch.stack(output)

"""Class that is responsible for training"""
class TetrisTrainer:
    def __init__(self, img_size):
        self.pil2tensor = transforms.ToTensor()
        self.img_size = img_size
        self.replay_memory = []

    """Converts a list of experiences into 4 tensors"""
    def experiences_to_tensor(self, experiences):
        #*experiences return all the elements as arguments in the experiences list
        #zip then returns an iterator which returns tuple i based on the ith element
        #for each input tuple return that as arguments to make a new Experience tuple
        batched_experience = Experience(*zip(*experiences))

        #Convert them into tensors
        #State is a tensor so we can just concat them
        state_tensors = torch.stack(batched_experience.state)
        next_state_tensors = torch.stack(batched_experience.next_state)

        #Action and reward are tuples hence need to convert them to tensors
        action_tensors = torch.from_numpy(np.asarray(batched_experience.action))
        reward_tensors = torch.from_numpy(np.asarray(batched_experience.reward))

        return state_tensors, action_tensors, reward_tensors, next_state_tensors
        
    """
    Given an image, process it into a suitable
    format for the network
    """
    def process_image(self, img):
        #Resize image
        img = img.resize(self.img_size)
        #Convert to grayscale
        img = ImageOps.grayscale(img)
        #Convert to tensor
        tensor = self.pil2tensor(img)
        tensor = tensor.squeeze(0)
        return tensor

    """
    Get the state of the environment
    by stacking multiple frames on top of each other
    """
    def get_state(self, env, frame_queue):
        frame = self.process_image(env.render())
        frame_queue.push(frame)
        return frame_queue.get_frames()

    """Get the gameover reward based on how long it survived"""
    def get_gameover_reward(self, timestep):
        #if timestep == 0:
        return base_gameover_reward
        #return base_gameover_reward / (survival_decay_rate * timestep)

    """Record an episode and push into memory"""
    def record_episode(self, agent, env, policy_net, episode_num):
        total_reward = 0
        time_steps_passed = 0
        #Reset environment
        env.reset()

        frame_queue = FrameQueue(3)

        #Get starting state
        state = self.get_state(env, frame_queue)
        #Run through the episode
        for timestep in range(max_episode_len):
            time_steps_passed += 1

            #Select an action
            action = agent.select_action(state, policy_net)
            #Get reward and if done
            _, reward, done, _ = env.step(action)

            if done:
                reward = self.get_gameover_reward(timestep)

            #Add to total reward
            total_reward += reward

            #Get next state
            next_state = self.get_state(env, frame_queue)

            #Store into memory
            self.replay_memory.push(Experience(state, action, reward, next_state))

            #Set next state to current state
            state = next_state

            #End episode if it finished
            if done:
                break
        print(f"Episode {str(episode_num)} Score: {str(round(total_reward,2))} Timesteps Survived: {str(time_steps_passed)}")

    """Training loop"""
    def train(self):
        #Get device
        device = torch.device('cpu')
        #Initialize replay memory
        self.replay_memory = ReplayMemory(memory_size)

        #Make strategy
        strategy = EpsilonGreedyStrategy(epsilon_start, epsilon_end, epsilon_decay)

        #Make environment
        env = gym.make('tetris-v0')

        num_actions = env.action_space.n

        #Make agent
        agent = TetrisAgent(strategy, num_actions, device)

        #Make policy network
        policy_net = TetrisDQN(game_img_size, num_actions).to(device)
        #Make target network
        target_net = TetrisDQN(game_img_size, num_actions).to(device)
        #Copy the weights from policy network to target network
        target_net.load_state_dict(policy_net.state_dict())
        #Set target network to evaluation mode so that the weights dont get updated
        target_net.eval()

        #Get optimizer
        optimizer = optim.Adam(params=policy_net.parameters(), lr=lr)

        #Get loss
        criterion = nn.MSELoss()

        #Train the episodes
        for episode in range(num_episodes):
            self.record_episode(agent, env, policy_net, episode)

            #Train DQN if possible
            if self.replay_memory.can_sample(batch_size):
                #Get experiences
                experiences = self.replay_memory.sample(batch_size)
                #Convert list of experiences into tensors
                states, actions, rewards, next_states = self.experiences_to_tensor(experiences)

                #Get current QValues for given states and actions
                curr_qvalues = QValue.get_current(policy_net, states, actions)
                #Calculate target values using bellman equation
                next_qvalues = QValue.get_next(target_net, next_states)
                target_qvalues = curr_qvalues + reward_discount * next_qvalues

                #Get loss
                loss = criterion(curr_qvalues, target_qvalues)
                #Zero the gradients for optimizer
                optimizer.zero_grad()
                #Backpropagate the tensors
                loss.backward()
                #Update values
                optimizer.step()

            #Update target network
            if episode % target_update == 0:
                target_net.load_state_dict(policy_net.state_dict())

            #Save model if necessary
            if episode % save_interval == 0:
                torch.save(policy_net.state_dict(), f'TetrisModel_eps_{episode}.pth')


if __name__ == '__main__':
    t = TetrisTrainer(game_img_size)
    t.train()